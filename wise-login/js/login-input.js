const inputs = document.getElementsByTagName('input');

for (var i = 0; i < inputs.length; i++) {
  inputs[i].addEventListener('click', function(e) {
    this.closest('.loginInput').classList.remove('borderDanger');
    document.getElementById('arrowBoxTopContainer').style.opacity = '0';
    this.closest('.loginInput').classList.add('active');
    e.stopPropagation();
  }, false);
}

document.body.addEventListener('click', function() {
  for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].value == '') {
      inputs[i].closest('.loginInput').classList.remove('active');
    }
  }
});
