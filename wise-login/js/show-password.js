const btnShowPassword = document.getElementById('showPassword');
const inputPassword = document.getElementById('password').querySelector('input');

btnShowPassword.addEventListener('click', function() {
  document.getElementById('showPassword').classList.toggle('active');

  if (inputPassword.type === "password") {
    inputPassword.type = "text";
  } else {
    inputPassword.type = "password";
  }
});
