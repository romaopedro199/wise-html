const btnForgetPassword = document.getElementById('forgetPassword');

btnForgetPassword.addEventListener('click', function() {
  document.getElementById('wrapper').classList.add('blur');
  document.getElementById('blurOpacity').classList.add('show');
  document.getElementById('modalForgetPassword').classList.add('show');
});

const btnForgetPasswordClose = document.getElementById('forgetPasswordClose');

btnForgetPasswordClose.addEventListener('click', function() {
  document.getElementById('wrapper').classList.remove('blur');
  document.getElementById('blurOpacity').classList.remove('show');
  document.getElementById('modalForgetPassword').classList.remove('show');
});

const btnForgetPasswordCancel = document.getElementById('forgetPasswordCancel');

btnForgetPasswordCancel.addEventListener('click', function() {
  document.getElementById('wrapper').classList.remove('blur');
  document.getElementById('blurOpacity').classList.remove('show');
  document.getElementById('modalForgetPassword').classList.remove('show');
});
