const btnNewPassword = document.getElementById('btnNewPassword');

btnNewPassword.addEventListener('click', function() {
  document.getElementById('wrapper').classList.add('blur');
  document.getElementById('blurOpacity').classList.add('show');
  document.getElementById('loading').classList.add('show');

  setTimeout(function(){
    document.getElementById('loading').classList.remove('show');
    document.getElementById('modalNewPasswordSuccess').classList.add('show');
    document.getElementById('newPasswordSuccessImg').classList.add('handToCenter');
  }, 1000);

  event.preventDefault();
});
